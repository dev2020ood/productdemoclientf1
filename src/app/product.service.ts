import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http'
import { Observable, Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  ResponseSubjects={
    "SaveProductOK":new Subject<any>(),
    "SaveProductInvalidID":new Subject<any>(),
    "SaveProductInvalidName":new Subject<any>()

      
    }

  
  constructor(private http:HttpClient) { 

  }
  SaveProduct(){
    var url = "api/products"
    //var url = "https://localhost:44340/api/products"  
    var saveData$ = this.http.post(url,{"ID":"12345","Name":"Oranges"})
    saveData$.subscribe(response=>this.ResponseSubjects[response["responseType"]].next(response))
    
      
  }
  OnSaveProductOK():Observable<any>
  {
      return this.ResponseSubjects["SaveProductOK"]
  }

  OnInvalidProductID():Observable<any>{
    return this.ResponseSubjects["SaveProductInvalidID"]
  }

  OnInvalidProductName():Observable<any>{
    return this.ResponseSubjects["SaveProductInvalidName"]
  }


}
