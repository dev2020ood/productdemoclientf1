import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import {map} from 'rxjs/operators'
import { ProductService } from '../product.service';


@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
  saveProduct$:Observable<any>
  constructor(private productService:ProductService) { }

  ngOnInit(): void {
    /*
    this.saveProduct$ = this.productService.SaveProduct()
    console.log("Init SaveProduct$")
    */
    this.productService.OnSaveProductOK().subscribe(response=>console.log("OK"))
    this.productService.OnInvalidProductID().subscribe(response=>console.log("InvalidID"))
    this.productService.OnInvalidProductName().subscribe(response=>console.log("InvalidName"))


  }
  SaveData(){
    this.productService.SaveProduct()
    /*
    var IDS$ = this.saveProduct$.pipe(
      map(response=>response.id)
    )
    this.saveProduct$
    .subscribe(resp=>console.log(resp),
    error=>console.log(error),
    ()=>console.log("Completed"))
   
     IDS$.subscribe(id=>console.log("ID is ==>",id)) 
     */

  }

}
